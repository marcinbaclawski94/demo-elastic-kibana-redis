package com.example.demo.books.domain;

import lombok.Value;

@Value
public class BookUpdateCommand {
  String id;
  String author;
  String title;
  String category;
}

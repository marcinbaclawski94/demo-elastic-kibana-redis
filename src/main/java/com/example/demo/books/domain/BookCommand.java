package com.example.demo.books.domain;

import lombok.Value;

@Value
public class BookCommand {
  String author;
  String title;
  String category;
}


package com.example.demo.books.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "book")
@Data
@NoArgsConstructor
public class Book {
  @Id
  private String id;
  private String author;
  private String title;
  private String category;

  public Book(String id, String author, String title, String category) {
    this.id = id;
    this.author = author;
    this.title = title;
    this.category = category;
  }

  public Book(BookCommand command) {
    this.id = null;
    this.author = command.getAuthor();
    this.title = command.getTitle();
    this.category = command.getCategory();
  }
}

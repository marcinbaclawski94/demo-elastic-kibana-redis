package com.example.demo.books.domain;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface BookRepository extends ElasticsearchRepository<Book, String > {


   Iterable<Book> findByTitle(String title);

   Iterable<Book> findByAuthor(String author);
}

package com.example.demo.books.web;

import com.example.demo.books.application.port.UseCaseBook;
import com.example.demo.books.domain.Book;
import com.example.demo.books.domain.BookCommand;
import com.example.demo.books.domain.BookUpdateCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/book")
@RequiredArgsConstructor
class BooksController {
  private final UseCaseBook useCaseBook;

  @GetMapping()
  List<Book> findAll() {
    return useCaseBook.findAll();
  }

  @GetMapping("/{id}")
  ResponseEntity<Book> findById(@PathVariable String id) {
    return useCaseBook.findById(id);
  }

  @GetMapping("/title/{title}")
  List<Book> findByTitle(@PathVariable String title) {
    return useCaseBook.findByTitle(title);
  }

  @GetMapping("/author/{author}")
  List<Book> findByAuthor(@PathVariable String author) {
    return useCaseBook.findByAuthor(author);
  }


  @PostMapping()
  void save(@RequestBody BookCommand command) {
    useCaseBook.save(command);
  }

  @DeleteMapping("/{id}")
  void delete(@PathVariable String id) {
    useCaseBook.deleteById(id);
  }
  @DeleteMapping()
  void delete() {
    useCaseBook.delete();
  }
  @PatchMapping()
  void update(@RequestBody BookUpdateCommand command) {
    useCaseBook.update(command);
  }
}

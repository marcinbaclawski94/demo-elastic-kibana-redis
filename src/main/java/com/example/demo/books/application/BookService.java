package com.example.demo.books.application;

import com.example.demo.books.application.port.UseCaseBook;
import com.example.demo.books.domain.Book;
import com.example.demo.books.domain.BookCommand;
import com.example.demo.books.domain.BookRepository;
import com.example.demo.books.domain.BookUpdateCommand;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
class BookService implements UseCaseBook {
  private final BookRepository repository;


  @Override
  public void save(BookCommand command) {
    repository.save(new Book(command));
  }

  @Override
  public ResponseEntity<Book> findById(String id) {
    final Optional<Book> book = repository.findById(id);
    return book.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
  }

  @Override
  public List<Book> findAll() {
    final Iterable<Book> booksIterable = repository.findAll();
    return changeToList(booksIterable);
  }

  private ArrayList<Book> changeToList(Iterable<Book> booksIterable) {
    final ArrayList<Book> books = new ArrayList<>();
    for (Book next : booksIterable) {
      books.add(next);
    }
    return books;
  }

  @Override
  public List<Book> findByTitle(String title) {
    val books = repository.findByTitle(title);
    return changeToList(books);
  }

  @Override
  public List<Book> findByAuthor(String author) {
    val books = repository.findByAuthor(author);
    return changeToList(books);
  }

  @Override
  public void deleteById(String id) {
    repository.deleteById(id);
  }

  @Override
  public void delete() {
    repository.deleteAll();
  }

  @Override
  public void update(BookUpdateCommand command) {
    val bookObject = repository.findById(command.getId());
    if (bookObject.isPresent()) {
      final Book book = bookObject.get();
      repository.delete(book);
      repository.save(new Book(command.getId(), command.getAuthor(), command.getTitle(), command.getCategory()));
    }

  }
}

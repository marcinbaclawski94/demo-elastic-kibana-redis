package com.example.demo.books.application.port;

import com.example.demo.books.domain.Book;
import com.example.demo.books.domain.BookCommand;
import com.example.demo.books.domain.BookUpdateCommand;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UseCaseBook {
  void save(BookCommand command);

  ResponseEntity<Book> findById(String id);

  List<Book> findAll();

  List<Book> findByTitle(String title);

  List<Book> findByAuthor(String author);

  void deleteById(String id);

  void delete();

  void update(BookUpdateCommand bookUpdate);
}

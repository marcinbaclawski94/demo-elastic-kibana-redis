package com.example.demo.config;

import com.example.demo.books.domain.Book;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedisConfig {
  @Bean
  public RedisTemplate<Integer, Book> redisTemplate(RedisConnectionFactory connectionFactory) {
    RedisTemplate<Integer, Book> template = new RedisTemplate<>();
    template.setConnectionFactory(connectionFactory);
    // Add some specific configuration here. Key serializers, etc.
    return template;
  }
}
